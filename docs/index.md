---
hide:
  - navigation
  - toc
---

# Yester Links Backup


Data from the great work of [sadgrlonline](https://github.com/sadgrlonline/yesterlinks).

Thanks :)



|title|url|descr|category|
|-----|---|-----|--------|
|DragCave|[https://dragcave.net/](https://dragcave.net/)|an adoptables game with dragons|fun|
|Emotional Codes|[https://emotional.codes/](https://emotional.codes/)|helpful resources for emotional problems|healing|
|Here and Now|[https://here-and-now.glitch.me/](https://here-and-now.glitch.me/)|an internet reflection space|healing|
|Feelu Now|[https://feelu.now.sh/](https://feelu.now.sh/)|an interactive wheel of emotions|healing|
|Bemuse|[https://bemuse.ninja/](https://bemuse.ninja/)|a browser-based rhythm game|fun|
|Tarot-o-Bot|[https://tarotobot-reboot.illo.tv/](https://tarotobot-reboot.illo.tv/)|a bot that pulls tarot cards|fun|
|KidPix|[https://kidpix.app/](https://kidpix.app/)|kidpix in your browser|fun|
|REMOJI|[https://remoji.com/](https://remoji.com/)|an emoji party in your browser|fun|
|Incarceration in Real Numbers|[https://mkorostoff.github.io/incarceration-in-real-numbers](https://mkorostoff.github.io/incarceration-in-real-numbers)|a visualization of incarceration in america|serious|
|Medical Bill Art|[https://medicalbill.art/](https://medicalbill.art/)|an art gallery made up of medical bills|serious|
|Snack Data|[https://snackdata.com/](https://snackdata.com/)|a website with data on every snack|fun|
|Kinopio|[https://kinopio.club/](https://kinopio.club/)|a tool for organizing workflows and ideas|useful|
|Domino|[https://kool.tools/domino](https://kool.tools/domino)|a tool for organizing spatial workflows/ideas|useful|
|Default File Name TV|[https://default-filename-tv.neocities.org/](https://default-filename-tv.neocities.org/)|a website that plays from youtube videos with the default filename|fun|
|An Idea for a Website|https://anideafora.website/|a generator that displays an idea for building a website when refreshed|fun|
|Sandspiel|[https://sandspiel.club/](https://sandspiel.club/)|create a mini ecosystem in your browser|fun|
|WinAmpify|[https://winampify.io/](https://winampify.io/)|listen to spotify through winamp on browser (premium only)|fun|
|Typohound|[https://typohound.com/](https://typohound.com/)|a tool that searches ebay by misspelled listings|useful|
|WinXP|[https://winxp.vercel.app/](https://winxp.vercel.app/)|windows xp in your browser|fun|
|iPod.js|[https://tannerv.com/ipod](https://tannerv.com/ipod)|an ipod classic in your browser|fun|
|YTMND|[https://ytmnd.com/](https://ytmnd.com/)|an iconic website from the early 00s|fun|
|Free Leftist Zines|[https://justsomeantifas.tumblr.com/zines](https://justsomeantifas.tumblr.com/zines)|a collection of free leftist zines available to download|serious|
|marxist.space|[https://marxist.space/](https://marxist.space/)|a list links to marxist resources|serious|
|Bead Letter Bracelet Maker|[https://o-o-o-o-o-o-o-o-o-o-o-o.net/](https://o-o-o-o-o-o-o-o-o-o-o-o.net/)|make a bead-letter bracelet in your browser|fun|
|Red Texts|[https://redtexts.org/](https://redtexts.org/)|communist text files|serious|
|PoolSuite|[https://poolsuite.net/](https://poolsuite.net/)|a retro-styled music player webpage|fun|
|WaifuLabs|[https://waifulabs.com/](https://waifulabs.com/)|generate anime girls with ai|fun|
|PhotoMosh|[https://photomosh.com/](https://photomosh.com/)|add glitchy gif effects to images|useful|
|9/11 Realtime|[https://911realtime.org/](https://911realtime.org/)|a multimedia experiment of 9/11 coverage in your browser|serious|
|Bus Stop|[https://bus-stop.net/](https://bus-stop.net/)|an anonymous message board|social|
|Gossip's Cafe|[https://gossips.cafe/](https://gossips.cafe/)|a chatroom that is only open certain times of the day|social|
|Untitled|[https://midnight.pub/](https://midnight.pub/)|No description added.|social|
|Multiverse Plus|[https://multiverse.plus/](https://multiverse.plus/)|a social website where you can create customized 'comics'|social|
|VentScape|[https://www.ventscape.life/](https://www.ventscape.life/)|an interactive website for venting|healing|
|SpaceHey|[https://spacehey.com/](https://spacehey.com/)|a fully operational and maintained myspace clone|social|
|Purrli|[https://purrli.com/](https://purrli.com/)|asmr with cat purrs|healing|
|How to Meditate|[https://www.dhammatalks.org/books/WithEachAndEveryBreath/Section0004.html](https://www.dhammatalks.org/books/WithEachAndEveryBreath/Section0004.html)|an introductory guide on how to meditate|healing|
|U.S. Atrocities|[https://github.com/dessalines/essays/blob/master/us_atrocities.md](https://github.com/dessalines/essays/blob/master/us_atrocities.md)|a list of atrocities committed by the u.s.|serious|
|U.S. Hate Crimes|[https://github.com/dessalines/essays/blob/master/us_hate_crimes.md](https://github.com/dessalines/essays/blob/master/us_hate_crimes.md)|a list of hate crimes committed by the u.s.|serious|
|MasterWiki|[https://masterwiki.how/](https://masterwiki.how/)|stolen from masterclass, republished as wikihow|useful|
|Comic Sans Appreciation Society|[https://www.comicsans.love/](https://www.comicsans.love/)|a website dedicated to appreciating comic sans|fun|
|Anticapitalism FAQ|[http://anticapitalismfaq.com/](http://anticapitalismfaq.com/)|an faq about anticapitalism|serious|
|Photopea|[https://photopea.com/](https://photopea.com/)|a photoshop clone for your browser|useful|
|Untools|[https://untools.co/](https://untools.co/)|a list of tools for better thinking|useful|
|MyNoise|[https://mynoise.net/noiseMachines.php](https://mynoise.net/noiseMachines.php)|a collection of noise machines to create your own background|useful|
|Tools for Creating Ideas|[http://creatingminds.org/tools/tools_ideation.htm](http://creatingminds.org/tools/tools_ideation.htm)|a list of ways to come up with ideas when you're stuck|useful|
|Post Office|[https://afternoon.dynu.com/](https://afternoon.dynu.com/)|an anonymous textboard for people who like to relax|social|
|The Park|[http://www.citypark.world/](http://www.citypark.world/)|an anonymous textboard|social|
|comfy.chat|[https://www.comfy.chat/](https://www.comfy.chat/)|a comfy anonymous themed imageboard|social|
|Postbox Garden|[https://postbox.garden/](https://postbox.garden/)|an imageboard for postcards|social|
|Acapella Extractor|[https://www.acapella-extractor.com/](https://www.acapella-extractor.com/)|extract an acapella from any song|useful|
|Essays on Capitalism|[https://github.com/dessalines/essays/blob/master/capitalism_doesnt_work.md](https://github.com/dessalines/essays/blob/master/capitalism_doesnt_work.md)|essays about capitalism|serious|
|Remove BG|[https://www.remove.bg/](https://www.remove.bg/)|remove the background from any image|useful|
|Text to ASCII|[https://patorjk.com/software/taag/](https://patorjk.com/software/taag/)|create ascii text from regular text|useful|
|Xhalr|[https://xhalr.com/](https://xhalr.com/)|a breathing guide for use with meditation and relaxation|healing|
|Pixel Thoughts|[https://pixelthoughts.co/](https://pixelthoughts.co/)|a 60-second meditation to help clear your mind|healing|
|Tree.FM|[https://www.tree.fm/](https://www.tree.fm/)|listen to a random forest|healing|
|What's on Netflix|[https://www.whats-on-netflix.com/library/](https://www.whats-on-netflix.com/library/)|an up-to-date database of which movies and shows are currently on netflix|useful|
|Blacklight|[https://themarkup.org/blacklight](https://themarkup.org/blacklight)|a realtime web-privacy inspector|useful|
|Polotno Studio|[https://studio.polotno.dev/](https://studio.polotno.dev/)|a graphics creation tool for the browser, similar to canva|useful|
|If It Were My Home|[https://www.ifitweremyhome.com/](https://www.ifitweremyhome.com/)|compare living conditions in your own country to those of another|serious|
|Wealth Shown to Scale|[https://mkorostoff.github.io/1-pixel-wealth](https://mkorostoff.github.io/1-pixel-wealth)|a graphic of wealth shown to scale|serious|
|Little Sis|[https://littlesis.org/](https://littlesis.org/)|a grassroots watchdog network connecting the dots between the world's most powerful people and organizations|serious|
|Randoma11y|[https://randoma11y.com/](https://randoma11y.com/)|randomly generated duotine high-contrast color combinations|useful|
|misty/cafe|[https://misty.cafe/](https://misty.cafe/)|cafe ambience and background music|healing|
|Self Care Guide|[https://philome.la/jace_harr/you-feel-like-shit-an-interactive-self-care-guide/play/index.html](https://philome.la/jace_harr/you-feel-like-shit-an-interactive-self-care-guide/play/index.html)|an interactive guide to self-care|healing|
|Muscle Wiki|[https://musclewiki.com/](https://musclewiki.com/)|a tool where you can select an area of your body and it will tell you which muscles are there (and how to stretch them)|useful|
|Color Palette Generator|[https://www.degraeve.com/color-palette](https://www.degraeve.com/color-palette)|generate a color palette from an image|useful|
|Native Land|[https://native-land.ca/](https://native-land.ca/)|an interactive map of indigenous nations|serious|
|NoClip|https://noclip.website/|explore iconic video game environments in your browser|fun|
|Listography|[https://listography.com/](https://listography.com/)|a social network for listmaking|useful|
|Self Keeping|[https://thedigitaldiarist.ca/writing/selfkeeping/thelittlethings.html](https://thedigitaldiarist.ca/writing/selfkeeping/thelittlethings.html)|a webpage for self-care and self-keeping tips|healing|
|Neocities|[https://neocities.org/](https://neocities.org/)|a free static website host, build your own website with html and css|social|
|Magnet Poetry|[https://sadgrl.online/magnet-poetry](https://sadgrl.online/magnet-poetry)|make magnet poetry in your browser|useful|
|Premium Wordpress Plugins|[https://gpldl.com/repository/premium-wordpress-plugins](https://gpldl.com/repository/premium-wordpress-plugins)|a database of premium wordpress plugins|useful|
|mmm.page|[https://build.mmm.page/](https://build.mmm.page/)|a drag and drop page builder in your browser|useful|
|hotglue.me|[https://hotglue.me/](https://hotglue.me/)|a drag and drop page builder in your browser|useful|
|Trianglify|[https://trianglify.io/](https://trianglify.io/)|generate a low-poly triangle pattern|useful|
|Crush Capitalism|[https://crushcapitalism.com/](https://crushcapitalism.com/)|simplified summaries and reading guides about capitalism|serious|
|Dollzmania|[https://dollzmania.neocities.org/](https://dollzmania.neocities.org/)|a website with resources to classic dollz graphics|fun|
|Glitter Graphics|[https://www.glitter-graphics.com/](https://www.glitter-graphics.com/)|a collection of old glitter graphics|useful|
|Put it on a Pedastal|[http://www.putitonapedestal.com/](http://www.putitonapedestal.com/)|a drag-and-drop pedestal game for your browser|fun|
|Ordered Dither Maker|[https://seleb.github.io/ordered-dither-maker](https://seleb.github.io/ordered-dither-maker)|a browser tool that will dither an image|useful|
|whimsy.space|[https://whimsy.space/](https://whimsy.space/)|an interactive browser-based operating system|fun|
|WinAmp Skins Museum|[https://skins.webamp.org/](https://skins.webamp.org/)|a collection of winamp skins for free use|useful|
|Just Trust Me|[https://matias.ma/nsfw](https://matias.ma/nsfw)|a fun website, nothing truly nsfw (aside from autoplay and tw: flashing colors).|fun|
|BeepBox|[https://www.beepbox.co/](https://www.beepbox.co/)|create and share music in your browser|useful|
|Goatlings|[https://www.goatlings.com/](https://www.goatlings.com/)|a virtual pet game in your browser|fun|
|Clozemaster|[https://www.clozemaster.com/languages](https://www.clozemaster.com/languages)|a tool for learning new languages|useful|
|CamelCamelCamel|[https://camelcamelcamel.com/](https://camelcamelcamel.com/)|a browser add-on that tracks amazon price history|useful|
|ZLibrary|[https://b-ok.cc/](https://b-ok.cc/)|a database of free downloadable books|useful|
|Just the Recipe|[https://www.justtherecipe.com/](https://www.justtherecipe.com/)|get just the instructions from any recipe, no life stories|useful|
|This to That|[http://www.thistothat.com/](http://www.thistothat.com/)|a tool to help identify bonding material for attaching one item to another|useful|
|Online OCR|[https://www.onlineocr.net/](https://www.onlineocr.net/)|a tool for extracting text from an image|useful|
|Every Noise|[https://everynoise.com/](https://everynoise.com/)|a huge map of every music genre|useful|
|Vimm's Lair|[https://vimm.net/?p=vault](https://vimm.net/?p=vault)|a collection of roms for retro video games|useful|
|Ad Link Bypasser|[https://thebypasser.com/](https://thebypasser.com/)|bypass links that require watching an ad before viewing|useful|
|Car Complaints|[https://www.carcomplaints.com/](https://www.carcomplaints.com/)|a database of common car issues sorted by make/model|useful|
|Fake Name Generator|[https://www.fakenamegenerator.com/](https://www.fakenamegenerator.com/)|generate a fake name and identity|useful|
|Future Me|[https://www.futureme.org/](https://www.futureme.org/)|write an email to your future self|fun|
|ShotCut|[https://shotcut.com/](https://shotcut.com/)|free video editing software for windows, mac and linux|useful|
|12ft|[https://12ft.io/](https://12ft.io/)|a proxy that will remove the paywall on most websites|useful|
|Scattergories Lists|[https://swellgarfo.com/scattergories](https://swellgarfo.com/scattergories)|a list generator for scattergories games|fun|
|Tip of my Tongue|[https://chir.ag/projects/tip-of-my-tongue](https://chir.ag/projects/tip-of-my-tongue)|a tool to help remember the word on the tip of your tongue|useful|
|Toffee Share|[https://toffeeshare.com/](https://toffeeshare.com/)|share files privately & securely, without a size limit|useful|
|Learn Anything|[https://learn-anything.xyz/](https://learn-anything.xyz/)|a search engine to find technical tutorials|useful|
|The Quiet Place|[https://thequietplaceproject.xyz/thequietplace](https://thequietplaceproject.xyz/thequietplace)|a relaxation project|healing|
|WTF should I do with my life|[https://www.wtfshouldidowithmylife.com/](https://www.wtfshouldidowithmylife.com/)|a generator that displays activities at random|fun|
|Make Sweet|[https://makesweet.com/](https://makesweet.com/)|create customized 3d graphics in your browser|useful|
|READ SETTLERS|[https://readsettlers.org/](https://readsettlers.org/)|a full-text copy of the book ||settlers: the mythology of the white proletariat|||serious|
|Bitmap Fonts|[http://www.dsg4.com/04/extra/bitmap/index.html](http://www.dsg4.com/04/extra/bitmap/index.html)|a collection of tiny bitmap fonts|useful|
|BitFontMaker|[https://pentacom.jp/pentacom/bitfontmaker2/](https://pentacom.jp/pentacom/bitfontmaker2/)|a bitmap font maker for your browser|useful|
|AutoChords|[https://autochords.com/](https://autochords.com/)|a tool for generating chords in a specific key|useful|
|Music Theory|[https://musictheory.net/](https://musictheory.net/)|an introductory interactive course on music theory|useful|
|Jummbus|[https://jummbus.bitbucket.io/](https://jummbus.bitbucket.io/)|make music in your browser|useful|
|Citizen DJ Labs|[https://citizen-dj.labs.loc.gov/](https://citizen-dj.labs.loc.gov/)|a collection of free-to-use sounds and mixing audio|useful|
|WebNeko|[https://webneko.net/](https://webneko.net/)|a script to add a little kitty that chases your cursor on a webpage|fun|
|GifyPet|[https://gifypet.neocities.org/](https://gifypet.neocities.org/)|an interactive virtual pet for your website|fun|
|posting.cool|[https://posting.cool/](https://posting.cool/)|an online message board|social|
|MelonLand Forum|[https://forum.melonking.net/](https://forum.melonking.net/)|an old-school-styled social forum|social|
|Yesterweb|[https://yesterweb.org/](https://yesterweb.org/)|a community dedicated to making the internet a better place|social|
|straw.page|[https://straw.page/](https://straw.page/)|a drag and drop page builder that works on mobile|useful|
|MSPFA|[https://mspfa.com/](https://mspfa.com/)|library of ms paint fan adventures (homestucklikes)|fun|
|Sadgrl.online|[https://sadgrl.online/](https://sadgrl.online/)|a personal site about the internet|personal|
|Seventh Sanctum|[https://www.seventhsanctum.com/](https://www.seventhsanctum.com/)|an idea generator website. good for creating new characters and getting prompts for stories.|useful|
|Wendy Carlos HomePage|[https://www.wendycarlos.com/](https://www.wendycarlos.com/)|permanently-90s homepage by synth pioneer wendy carlos, with information on her music and other passions.|personal|
|Short Trip|[https://alexanderperrin.com.au/paper/shorttrip/#](https://alexanderperrin.com.au/paper/shorttrip/#)|beautiful hand-drawn animated tram ride. just hold left or right to move. stop at the station to let the hand drawn cute cats to sit inside!|healing|
|Pronoun Dressing Room|[http://www.pronouns.failedslacker.com/](http://www.pronouns.failedslacker.com/)|test different pronouns to discover what feels most comfortable!|useful|
|myWriteClub|[https://www.mywriteclub.com/beta/](https://www.mywriteclub.com/beta/)|keep track of your progress as you work toward your writing goals. you can work solo or with others!|useful|
|Perchance|[https://perchance.org/welcome](https://perchance.org/welcome)|perchance is a platform for creating and sharing random text generators.|fun|
|Web Gems|[https://webgems.io/](https://webgems.io/)|hundreds of educational resources and useful tools for web development|useful|
|Crouton|[https://crouton.net/](https://crouton.net/)|crouton.|fun|
|AI Dungeon|[https://play.aidungeon.io/](https://play.aidungeon.io/)|an ai-generated interactive text adventure|fun|
|aggie.io|[https://aggie.io/](https://aggie.io/)|a collaborative painting tool|social|
|Sampulator|[http://sampulator.com/](http://sampulator.com/)|a garage-band style sequencer for your browser|useful|
|DownThemAll|[https://www.downthemall.net/](https://www.downthemall.net/)|a browser extension for downloading every graphic from a webpage|useful|
|HTTrack|[https://www.httrack.com/](https://www.httrack.com/)|software for windows/linux that will crawl and locally archive a website for you|useful|
|Single File|[https://github.com/gildas-lormeau/SingleFile](https://github.com/gildas-lormeau/SingleFile)|a browser extension that will save an entire website locally as a .html file.|useful|
|PorkBun|[https://porkbun.com/](https://porkbun.com/)|cheap domains with free domain privacy and email forwarding|useful|
|SCMPlayer|[https://www.scmplayer.net/](https://www.scmplayer.net/)|a music player that automatically loads across of your website pages|useful|
|Web Badges World|[https://web.badges.world/](https://web.badges.world/)|a huge collection of web badges to display on your site|useful|
|Cheap VPS Hosts|[https://bytemoth.neocities.org/dive/lebrevps.txt](https://bytemoth.neocities.org/dive/lebrevps.txt)|a list of some of the cheapest vps hosting available|useful|
|TinyKVM|[https://tinykvm.com/](https://tinykvm.com/)|a very small, cheap kvm host|useful|
|Pollcode|[https://pollcode.com/](https://pollcode.com/)|a site to make your own polls for your website viewers|useful|
|Freecycle|[https://www.freecycle.org/](https://www.freecycle.org/)|a website that hosts a place for you to locally list something you're looking for, or giving away for free. usually done via pickups and dropoffs|useful|
|FogCam!|[https://fogcam.org/](https://fogcam.org/)|the world's oldest webcam. live san francisco views since 1994.|fun|
|Gabe's site|[https://gabe.rocks/](https://gabe.rocks/)|a small space for my passions|personal|
|Nose Club|[https://noseclub.bluwiikoon.art/](https://noseclub.bluwiikoon.art/)|a pokemon fansite with cute art, fanfics, art guides, and other pokemon-y things.|fun|
|Yesterweb Social|[https://social.yesterweb.org/](https://social.yesterweb.org/)|a mastodon server for the yesterweb|social|
|Yesterweb Gemini Proxy|[https://yesterweb.org/gemini.html](https://yesterweb.org/gemini.html)|a proxy that can load gemini:// pages over https|useful|
|Interneting is Hard|[https://www.internetingishard.com/](https://www.internetingishard.com/)|web development tutorials for complete beginners|useful|
|Toonami Aftermath|[https://www.toonamiaftermath.com/](https://www.toonamiaftermath.com/)|a website that streams old toonami shows (and other cartoons) 24/7|fun|
|Old Version|[http://www.oldversion.com/](http://www.oldversion.com/)|locate old versions of software|useful|
|EZ Gif|[https://ezgif.com/](https://ezgif.com/)|create and edit gifs in your browser|useful|
|Lovely Designs|[http://www.lovelyd.shukuya.com/](http://www.lovelyd.shukuya.com/)|an old school layout site|useful|
|boxboxhtml|[https://seansleblanc.itch.io/boxboxhtml](https://seansleblanc.itch.io/boxboxhtml)|a tool to help design a website layout that exports to html|useful|
|cbox|[https://www.cbox.ws/](https://www.cbox.ws/)|a free chatbox that can be embedded on a website|social|
|HALCYON Layouts|[https://halcyon.candyrain.org/LAYOUTS/layouts.php](https://halcyon.candyrain.org/LAYOUTS/layouts.php)|old-school style anime layouts for websites|useful|
|TooPlate|[https://www.tooplate.com/](https://www.tooplate.com/)|modern html/css templates for free usage|useful|
|Learn Webcrafting|[https://learn.sadgrl.online/](https://learn.sadgrl.online/)|a series of website building guides by sadness|useful|
|Bitsy|[http://ledoux.io/bitsy/editor.html](http://ledoux.io/bitsy/editor.html)|an editor for creating minimal video games|useful|
|Escargot|[https://escargot.chat/](https://escargot.chat/)|a service that makes msn messenger work again|social|
|Marginalia|[https://search.marginalia.nu/](https://search.marginalia.nu/)|an independent search engine for non-commercial content|useful|
|Wiby|[https://wiby.me/](https://wiby.me/)|a search engine for the classic web|useful|
|GifCities|[https://gifcities.org/](https://gifcities.org/)|a collection of archived gifs from geocities|fun|
|WebPage List|[https://webpagelist.com/](https://webpagelist.com/)|a list of webpages and free resources|useful|
|Flexbox Froggy|[https://flexboxfroggy.com/](https://flexboxfroggy.com/)|a game to assist with learning flexbox|useful|
|CSS Grid Garden|[https://cssgridgarden.com/](https://cssgridgarden.com/)|a game for learning how to use css grid|useful|
|VS Code Pets|[https://marketplace.visualstudio.com/items?itemName=tonybaloney.vscode-pets](https://marketplace.visualstudio.com/items?itemName=tonybaloney.vscode-pets)|an extension for vs code which gives you a little pet|fun|
|Piracy Megathread|[https://notabug.org/TheChumBucket/PiracySubreddit/src/master/wiki/megathread.md](https://notabug.org/TheChumBucket/PiracySubreddit/src/master/wiki/megathread.md)|a megathread about piracy|useful|
|Stress Analyst|[http://www.relaxonline.me.uk/sa1/index.html](http://www.relaxonline.me.uk/sa1/index.html)|if you've just had a stressful experience then this interactive page will help you calm down and get over it|healing|
|Mental Health Resources|[https://mentalillnessmouse.wordpress.com/helpfulresources/#_=_](https://mentalillnessmouse.wordpress.com/helpfulresources/#_=_)|a large list of mental health resources|healing|
|Crystal Ball|[https://www.oproot.com/ball/](https://www.oproot.com/ball/)|a crystal ball with wisdom from beyond|fun|
|Velvetyne|[http://velvetyne.fr/](http://velvetyne.fr/)|free and open source fonts|useful|
|Web Design Museum|[https://www.webdesignmuseum.org/](https://www.webdesignmuseum.org/)|a catalog of trends in old web design|fun|
|FlowCV|[https://flowcv.io/](https://flowcv.io/)|free cv builder in your browser|useful|
|Poly Pizza|[https://poly.pizza/](https://poly.pizza/)|a database of free-to-use low-poly graphics|useful|
|fLaMEdFury|[https://flamedfury.com/](https://flamedfury.com/)|a collection of stuff that means everything to me and probably nothing to you!|personal|
|Bulk Resize Photos|[https://bulkresizephotos.com/](https://bulkresizephotos.com/)|a free way to quickly bulk resize photos. it will even add padding to make sure the image isn't squished at those dimensions!|useful|
|Yesterweb Live Radio|[https://yesterweb.org/radio](https://yesterweb.org/radio)|a free internet radio that plays indie music and internet-related interviews and programming|fun|
|Sadgrl's 88x31 Button Maker|[https://sadgrl.online/projects/88x31-button-maker.html](https://sadgrl.online/projects/88x31-button-maker.html)|a browser-based tool to help you create a custom 88x31 button|useful|
|Gossip's Web|[https://gossipsweb.net/](https://gossipsweb.net/)|a directory of independent and creative websites|useful|
|The Models Resource|[https://www.models-resource.com/](https://www.models-resource.com/)|a website where you can download and study 3d models, textures, and sounds from video games! very useful for analyzing low poly modeling.|useful|
|Video Ascii Art|[http://www.kickjs.org/example/video_ascii_art/Video_Ascii_Art.html](http://www.kickjs.org/example/video_ascii_art/Video_Ascii_Art.html)|play a video as ascii art!|fun|
|StumblingOn|[https://stumblingon.com/](https://stumblingon.com/)|click on 'stumble' to get a random webpage|useful|
|Glitch|[https://glitch.com/](https://glitch.com/)|a web hosting service that offers unlimited free static websites|useful|
|SomaFM|[https://somafm.com/](https://somafm.com/)|commercial-free underground/alternative radio|fun|
|Who Pays Writers|[http://whopayswriters.com/#/results](http://whopayswriters.com/#/results)|an anonymous, crowd-sourced list of which publications pay freelance writers - and how much|useful|
|massline.info|[http://massline.info/](http://massline.info/)|a site devoted to the mass line and having a mass perspective (maoism)|serious|
|Climate Explorer|[https://crt-climate-explorer.nemac.org/](https://crt-climate-explorer.nemac.org/)|explore how climate is projected to change in any county in the united states|serious|
|Memory Work|[https://memory-work.com/](https://memory-work.com/)|a research-based scenario, indicating possible futures of women’s labour|serious|
|A Book Like Foo|[https://abooklikefoo.com/escape](https://abooklikefoo.com/escape)|break your literary echo chamber by finding books you are statistically unlikely to read|useful|
|Reverse Interview|[https://github.com/viraptor/reverse-interview](https://github.com/viraptor/reverse-interview)|a list of interview questions which may be interesting to a tech job applicant|useful|
|CSS Puns|[https://saijogeorge.com/css-puns/](https://saijogeorge.com/css-puns/)|puns about css|fun|
|This X Does Not Exist|[https://thisxdoesnotexist.com/](https://thisxdoesnotexist.com/)|a collection of ai generated photos of... everything you can possibly imagine|useful|
|Learning Music|[https://learningmusic.ableton.com/](https://learningmusic.ableton.com/)|an interactive course that teaches you how to make music|useful|
|Subreddit Stats|[https://subredditstats.com/](https://subredditstats.com/)|statistics for every subreddit|useful|
|Dungeon Scrawl|[https://probabletrain.itch.io/dungeon-scrawl](https://probabletrain.itch.io/dungeon-scrawl)|an interactive tool for mapping dungeon layouts|useful|
|Screen Share Party|https://[ba.net/screen-share-party/#6220874879569449](ba.net/screen-share-party/#6220874879569449)|a free no-install no-registration screensharing app|social|
|href.cool|[https://href.cool/](https://href.cool/)|a curated list of interesting and unique websites|useful|
|Ninfex|[https://www.ninfex.com/](https://www.ninfex.com/)|a ||people powered|| search engine, powered by user submissions|useful|
|Diabella Loves Cats|[http://diabellalovescats.com/](http://diabellalovescats.com/)|vintage cat graphics since 1997|fun|
|Perpetual Calendar by Olia Lialia|[https://haveagood.today/](https://haveagood.today/)|an infinite calendar of cheesy vintage web graphics|fun|
|Geocities Midi Collection|[https://archive.org/details/archiveteam-geocities-midi-collection-2009](https://archive.org/details/archiveteam-geocities-midi-collection-2009)|a collection of midis archived from geocities|fun|
|SNES Soundfonts|[https://www.williamkage.com/snes_soundfonts/](https://www.williamkage.com/snes_soundfonts/)|downloads of soundfonts from snes games|fun|
|Death Generator|[https://deathgenerator.com/](https://deathgenerator.com/)|a website where you can put custom text over classic video game dialogue images|fun|
|Erowid|[https://erowid.org/](https://erowid.org/)|a classic website with information about various drugs|useful|
|Ichigo Directory|[https://directory.cinni.net/](https://directory.cinni.net/)|a directory of cute pixel graphics websites|fun|
|Goatcounter|[https://www.goatcounter.com/](https://www.goatcounter.com/)|a privacy-friendly alternative to google analytics for your website|useful|
|RiseUp VPN|[https://riseup.net/vpn](https://riseup.net/vpn)|a free vpn for windows, mac and linux|useful|
|ilai.link|[http://www.ilai.link/](http://www.ilai.link/)|super futuristic website by the artist 11ai with endless pages 2 explore. a lot of music/sound making games and net art pieces like music visualizers.|fun|
|Notion.so|[https://notion.so/](https://notion.so/)|an free cloud-based online notetaking and knowledge management tool|useful|
|CARI Institute|https://cari.institute/|consumer aesthetics research institute, a website that catalogs aesthetics by year|useful|
|Protopage|[https://www.protopage.com/](https://www.protopage.com/)|customize your own browser start page for free|useful|
|Animated Gif Net|[https://www.animatedgif.net/index.html](https://www.animatedgif.net/index.html)|a old collection of gifs~|useful|
|lime360 entertainment|[https://lime360.neocities.org/](https://lime360.neocities.org/)|lime360's homepage, with some fun stuff|personal|
|98.css|[https://jdan.github.io/98.css/](https://jdan.github.io/98.css/)|a windows 98 ui made with css|fun|
|XP.css|[https://botoxparty.github.io/XP.css/](https://botoxparty.github.io/XP.css/)|a windows xp ui made with css|fun|
|7.css|[https://khang-nd.github.io/7.css/](https://khang-nd.github.io/7.css/)|a windows 7 ui made with css|fun|
|ThemeKings|[http://themekings.net/](http://themekings.net/)|old school html and css layouts|useful|
|Layoutit Grid|[https://grid.layoutit.com/](https://grid.layoutit.com/)|a visual interactive tool to create layouts with css grid|useful|
|bashblog|[https://github.com/cfenollosa/bashblog](https://github.com/cfenollosa/bashblog)|a highly customizable blogging tool for neocities (supporter only)|useful|
|Odie|[http://odie.us/](http://odie.us/)|create a website with a public google doc|useful|
|Webmaker.app|[https://webmaker.app/](https://webmaker.app/)|an offline alternative to codepen|useful|
|Old School PC Fonts|[https://int10h.org/oldschool-pc-fonts/](https://int10h.org/oldschool-pc-fonts/)|an old-school pc font pack|fun|
|TiddlyWiki|[https://tiddlywiki.com/](https://tiddlywiki.com/)|a static html wiki builder|useful|
|You Don't Need JS|[https://github.com/you-dont-need/You-Dont-Need-JavaScript](https://github.com/you-dont-need/You-Dont-Need-JavaScript)|a list of javascript-y things you can do with css|useful|
|Neocities Profile Bookmarklet|[https://jameso2.neocities.org/tools/neocities-profile-Bookmarklet.html](https://jameso2.neocities.org/tools/neocities-profile-Bookmarklet.html)|a bookmarket that makes it easy to view neocities profiles (if no custom domain)|useful|
|Github Pages|[https://pages.github.com/](https://pages.github.com/)|create static websites from github repos|useful|
|PicMix|[https://en.picmix.com/](https://en.picmix.com/)|a graphics glitter-ifyer, good alternative to blingee|fun|
|Smileys|[http://www.mazeguy.net/smilies.html](http://www.mazeguy.net/smilies.html)|a collection of old smileys|fun|
|Internet Bumper Stickers|[http://www.internetbumperstickers.com/](http://www.internetbumperstickers.com/)|a collection of bumper sticker graphics for your website|fun|
|Tamanotchi World|[https://tamanotchi.world/](https://tamanotchi.world/)|virtual pets you can embed on your website|fun|
|Transparent Textures|[https://www.transparenttextures.com/](https://www.transparenttextures.com/)|a filterable collection of transparent textures|useful|
|Repeater|[https://www.richardwestenra.com/repeater/](https://www.richardwestenra.com/repeater/)|create tiled backgrounds from images in your browser|useful|
|ArchiveBox|[https://github.com/ArchiveBox/ArchiveBox](https://github.com/ArchiveBox/ArchiveBox)|a tool for open-source self-hosted web archiving|useful|
|TumblThree|[https://github.com/TumblThreeApp/TumblThree](https://github.com/TumblThreeApp/TumblThree)|a tool for archiving all data from tumblr and twitter accounts in one click|useful|
|Excalidraw|[https://excalidraw.com/](https://excalidraw.com/)|a virtual collaborative whiteboard|social|
|PixelArt CSS|[https://www.pixelartcss.com/](https://www.pixelartcss.com/)|a tool to create pixel art and then convert it to css|fun|
|Succeed Socially|[https://www.succeedsocially.com/](https://www.succeedsocially.com/)|a free guide on social skills and overcoming shyness and insecurities for adults and older teenagers|useful|
|Cookie Consent Speedrun|[https://cookieconsentspeed.run/](https://cookieconsentspeed.run/)|(try to) reject all cookies|fun|
|Skytopia|[https://www.skytopia.com/](https://www.skytopia.com/)|a colorful webpage to explore and discover unique things|fun|
|Bloom3D|[https://bloom3d.com/](https://bloom3d.com/)|create quick and easy 3d designs in your browser|useful|
|emojitracker|[https://emojitracker.com/](https://emojitracker.com/)|see the most popular emoji used in realtime (based on twitter data)|fun|
|Two Cans and String|[https://twocansandstring.com/](https://twocansandstring.com/)|ask and answer questions anonymously, and chat with random people (sign up required)|social|
|Wilderness Land|[https://wilderness.land/](https://wilderness.land/)|a website that's a giant map with over 500 interesting or unique websites hidden as links throughout it|fun|
|status.cafe|[https://status.cafe/](https://status.cafe/)|your friends' status|social|
|Drawing Garden|[https://drawing.garden/](https://drawing.garden/)|a small web app to help you relax while you draw a garden|healing|
|Texture Town|[https://textures.neocities.org/](https://textures.neocities.org/)|textures for websites and 3d worlds|useful|
|A Random Site|[https://arandomsite.neocities.org/](https://arandomsite.neocities.org/)|the most random est site ever.|personal|
|Neal Fun|[https://neal.fun/](https://neal.fun/)|a webpage featuring a variety of interactive content|fun|
|Map of Reddit|[https://anvaka.github.io/map-of-reddit/?x=255000&y=381000&z=1231248.9168102785](https://anvaka.github.io/map-of-reddit/?x=255000&y=381000&z=1231248.9168102785)|an interactive map of subreddits|fun|
|Metabook|[https://metabook.fyi/](https://metabook.fyi/)|a web statement against meta (aka facebook)|serious|
|Approved, or Not Approved|[https://approvednotapproved.com/](https://approvednotapproved.com/)|a project to to help understand how advertising guidelines might disservice your needs|serious|
|Eeerik|[http://eeerik.com/](http://eeerik.com/)|a really cool interactive web portal designed like an os|fun|
|Deleted City|[http://deletedcity.net/](http://deletedcity.net/)|an interactive map of geocities|fun|
|How Many Plants|[https://howmanyplants.com/](https://howmanyplants.com/)|an illustrated ad-free guide to plant care|useful|
|Cyberfeminism Index|[https://cyberfeminismindex.com/about/](https://cyberfeminismindex.com/about/)|an in-progress online collection of resources for techno-critical works from 1990–2020|serious|
|Food Timeline|[https://www.foodtimeline.org/](https://www.foodtimeline.org/)|a comprehensive timeline of food history|fun|
|Nobody Live|[https://nobody.live/](https://nobody.live/)|a website that shows links to streams where no one is watching|fun|
|Panther|[http://panther.audio/](http://panther.audio/)|discover new music through infinite recommendations|useful|
|Ryan's hallways|[https://vastrecs.neocities.org/](https://vastrecs.neocities.org/)|personal site with artists, music, stories, and links that i like except it's also written like you're in a strange hallway. also has a page for pinoy language and literature and a tma fansite (both under construction)|personal|
|Blinki.es|[https://blinki.es/](https://blinki.es/)|blinkies resource, has been operating since 2010!|useful|
|Magma studio|[https://magmastudio.io/](https://magmastudio.io/)|a browser art collaboration tool (similar to aggie.io)|social|
|Whiteboard fox|[https://r3.whiteboardfox.com/](https://r3.whiteboardfox.com/)|a browser collaboration tool with limited colours.|social|
|Archives Design|[https://archives.design/](https://archives.design/)|a digital archive of graphic design related items|useful|
|HTFMAT|[https://francescoimola.github.io/htfmat/](https://francescoimola.github.io/htfmat/)|is part of an ongoing project articulating personal ideas of home, what (and who) makes one, and how to find meaning living inbetween homes|healing|
|Grotto|[https://grotto.wileywiggins.com/](https://grotto.wileywiggins.com/)|an experimental web space that treats html pages as rooms that human players navigate in real time|social|
|After Dark CSS|[https://www.bryanbraun.com/after-dark-css/](https://www.bryanbraun.com/after-dark-css/)|a collection of old after dark screensavers... in css|fun|
|punkfairie|[https://punkfairie.net/](https://punkfairie.net/)|personal home page|personal|
|Cursor Effects|[https://tholman.com/cursor-effects/](https://tholman.com/cursor-effects/)|a website dedicated to modernization of 90s cursor effects (mobile friendly)|fun|
|Virtual Care Lab|[https://virtualcarelab.com/](https://virtualcarelab.com/)|a series of creative experiments in remote togetherness|healing|
|twelve.chat|[http://twelve.chat/](http://twelve.chat/)|an interactive memoir about a 12 year old in an aol chatroom (nfsw)|fun|
|La Capi, Interestelar Captian|[http://lacapi.tv/](http://lacapi.tv/)|a space carrier for the most trivial, short and routine travels, in search of adventures is lost in the ends of the sidereal space without almost any possibility of going home.|fun|
|Web Dev Toolkit|[https://toolkit.addy.codes/](https://toolkit.addy.codes/)|hundreds of curated web design & development resources|useful|
|SVG Pattern Generator|[https://app.haikei.app/](https://app.haikei.app/)|an app that generates svg patterns you can use on your website|useful|
|Archetype|[https://archetypeapp.com/](https://archetypeapp.com/)|an app to help you choose font size and spacing for your website on a live mockup|useful|
|glitterphoto|[https://glitterphoto.net/](https://glitterphoto.net/)|make a custom glitter graphic|fun|
|Alt-H|[https://alt-h.net/](https://alt-h.net/)|a collection of information and resources on the alterhuman subculture|useful|
|EVER17|[https://ever17.neocities.org/](https://ever17.neocities.org/)|a 2000s vn-inspired personal page including a multitude of just-for-fun or outright strange features.|fun|
|Space Jam|[https://www.spacejam.com/1996/](https://www.spacejam.com/1996/)|original website for the movie space jam, unchanged since 1996|fun|
|CSS FPS|[https://keithclark.co.uk/labs/css-fps/](https://keithclark.co.uk/labs/css-fps/)|an explorable, first-person, 3d environment built entirely with html and css|fun|
|A Field Guide to Web Accessibility|[https://theultimatemotherfuckingwebsite.com/](https://theultimatemotherfuckingwebsite.com/)|suggestions on making your website accessible|serious|
|Web Zine 01|[https://solarpunk.cool/zines/web-zine-01/table-of-contents.html](https://solarpunk.cool/zines/web-zine-01/table-of-contents.html)|a beginner's guide to making your first website|useful|
|The Fat Strawberry|[http://www.fatstrawberry.co.uk/](http://www.fatstrawberry.co.uk/)|a directory of seamless tiling textures|useful|
|Glittereyes|[http://ge.silentears.net/](http://ge.silentears.net/)|a resource for glitter graphics and glitter fills|fun|
|Poets' Corner|[https://www.theotherpages.org/poems/index.html](https://www.theotherpages.org/poems/index.html)|simple, old, elegant, and ad-free website full of poetry|fun|
|COMPOST|https://two.compost.digital/|an e-magazine dedicated to renewing our relationship with the web|fun|
|Disroot|[https://disroot.org/](https://disroot.org/)|a platform providing online services based on principles of freedom, privacy, federation and decentralization|useful|
|The Amazon Pamphlet|[https://xandra.cc/amazon.html](https://xandra.cc/amazon.html)|xandra's guide to amazon alternatives|useful|
|Digital Wellbeing|[https://omoulo.neocities.org/digitalwellbeing.html](https://omoulo.neocities.org/digitalwellbeing.html)|lauren's guide to healthy technology and social media habits|healing|
|Speak Up & Stay Safe(r)|[https://onlinesafety.feministfrequency.com/en/](https://onlinesafety.feministfrequency.com/en/)|a guide to protecting yourself from online harrassment|serious|
|Unconsenting Media|[https://www.unconsentingmedia.org/](https://www.unconsentingmedia.org/)|content warnings for tv shows, movies, and books|serious|
|Torrenting Guide|[https://y2kid.xyz/downloading.html](https://y2kid.xyz/downloading.html)|y2kid's guide to torrenting and downloading responsibly (eyestrain warning)|useful|
|Just Delete Me|[https://justdeleteme.xyz/](https://justdeleteme.xyz/)|a directory of direct links to delete your account from web services|useful|
|Forvo|[https://forvo.com/](https://forvo.com/)|pronunciation dictionary|useful|
|Etymonline|[https://www.etymonline.com/](https://www.etymonline.com/)|etymology dictionary|useful|
|Any Decent Music|[http://www.anydecentmusic.com/](http://www.anydecentmusic.com/)|music charts that are manually reviewed and aggregated|fun|
|Radio Browser|[https://www.radio-browser.info/](https://www.radio-browser.info/)|find live music streams all over the internet|fun|
|Net Art Anthology|[https://anthology.rhizome.org/](https://anthology.rhizome.org/)|retelling the history of net art from the 1980s to the 2010s.|fun|
|Whimsical !|[http://whimsical.heartette.net/](http://whimsical.heartette.net/)|web graphics and pixel art website !|fun|
|Razorback|[http://razorback95.com/](http://razorback95.com/)|website dictated to old computers|fun|
|James Clear - Great Talks Most People Have Never H|[https://jamesclear.com/great-speeches?utm_campaign=Sunday%20Synthesis&utm_medium=email&utm_source=Revue%20newsletter](https://jamesclear.com/great-speeches?utm_campaign=Sunday%20Synthesis&utm_medium=email&utm_source=Revue%20newsletter)|a page containing a list of inspirational and insightful talks from the prominent 21st-century author, james clear.|useful|
|JACKHOUSE Radio|[https://jackhouseradio.neocities.org/index.html](https://jackhouseradio.neocities.org/index.html)|an internet alternative music station made by jason osorio (jasonsworld)|useful|
|Monsterland|[https://monsterland.net/](https://monsterland.net/)|a website for collaborative drawing, no sign-up required|social|
|Rosetta Code|[https://www.rosettacode.org/wiki/Rosetta_Code](https://www.rosettacode.org/wiki/Rosetta_Code)|rosetta code exists to present solutions to the same programming task in as many different languages as possible.|useful|
|Queer Punk Bands Edinburgh|[https://dalmationer.art/music/punklist.html](https://dalmationer.art/music/punklist.html)|a list of many indie feminist and queer punk bands in edinburgh, scotland! a good resource if you want to book a gig, or listen to some new music.|fun|
|Background Tiles|[https://background-tiles.com/index.php](https://background-tiles.com/index.php)|tons of tiled backgrounds of all sorts of colors, textures and patterns!|useful|
|Amyot's Green Website|[https://temaisgame.neocities.org/](https://temaisgame.neocities.org/)|a website of amyot.|personal|
|Grays Tea|[https://graystea.neocities.org/](https://graystea.neocities.org/)|a personal page and a small community, grays tea purpose is to connect people and resources. this site consists of an archive of links including software, online tools, webdev educational resources, unique personal sites, and more.|useful|
|Well Observe|[https://www.wellobserve.com/](https://www.wellobserve.com/)|my place for drawings and paintings and random posts|personal|
|Steve Barnes' World Of Happiness|[https://stevebarnes.org/](https://stevebarnes.org/)|a neighbour's dwelling since the 1990s.  modern web standards, traditional web spirit.|personal|
|machine_cat in space|[https://machine-cat.space/](https://machine-cat.space/)|a machine and a cat in space! a personal homepage|personal|
|Cosmos BBS|[https://cosmos.opengateway.xyz/](https://cosmos.opengateway.xyz/)|classic bbs brought to you with a very web 1.0 style.|social|
|Pal - Constraint Systems|https://pal.constraint.systems/|pal let's you apply an eight-color terminal color palette to an image. use the keyboard controls to choose a theme, set thresholds, and cycle hues.|useful|
|Picrew|[https://picrew.me/](https://picrew.me/)|japanese language site with hundreds of user submitted doll creators|fun|
|88x31 GIF collection|[https://cyber.dabamos.de/88x31/index.html](https://cyber.dabamos.de/88x31/index.html)|just a collection of 88x31 gifs u can take|useful|
|Design Doll|[https://terawell.net/index.php#download](https://terawell.net/index.php#download)|a downloadable posing doll software for artist reference. allows you to change its shape and size and even import props. its free but it has a paid version.|useful|
|Cryptal Archive|[https://cryptalarchive.carrd.co/#](https://cryptalarchive.carrd.co/#)|a hub for everything about the web comic cryptal archive|personal|
|blinkies.cafe|[https://blinkies.cafe/](https://blinkies.cafe/)|blinkie generator - make blinkies with custom text|fun|
|Smol Pub|[https://smol.pub/](https://smol.pub/)|your journal on the small net|social|
|vpub • a retro but modern forum software|[https://vpub.miso.town/](https://vpub.miso.town/)|a retro but modern forum software|social|
|USTVGO|[https://ustvgo.tv/](https://ustvgo.tv/)|free cable tv (afaik you most likely need an adblocker!)|useful|
|Newgrounds|[https://newgrounds.com/](https://newgrounds.com/)|a flash portal website|fun|
|SethThyer's Webpage|[https://seththyer.com/](https://seththyer.com/)|a personal page for me to share thoughts and other things.|personal|
|Land Chad|[https://landchad.net/](https://landchad.net/)|an open-source collaborative tutorial website on all things internet.|useful|
|The Decolonial Atlas|[https://decolonialatlas.wordpress.com/](https://decolonialatlas.wordpress.com/)|a growing collection of maps which, in some way, help us to challenge our relationships with the land, people, and state|serious|
|Peer2Peer|[https://peer2peer.live/](https://peer2peer.live/)|peer2peer.live is an opt-in discoverability tool for marginalized streamers and viewers to find each other through robust identity-based tagging|social|
|karlherrick.com|[https://karlherrick.com/](https://karlherrick.com/)|karlherrick.com|personal|
|Scrubhaus|[http://www.scrubhaus.com/watch/](http://www.scrubhaus.com/watch/)|an underground movie club that streams sometimes|fun|
|FontStruct|[https://fontstruct.com/](https://fontstruct.com/)|with fontstruct you can easily create fonts using geometrical shapes and export to font.|useful|
|Alexis Gaming Reviews|[https://alexisgaming95.neocities.org/](https://alexisgaming95.neocities.org/)|my blog and some stuff. it includes stuff like a comment box, guestbook, wallpapers, rss feeds and more! made in microsoft frontpage 98, edited in neocities editor. i also made cool buttons :)|personal|
|Explore our Moon!|[https://blog.jatan.space/our-moon](https://blog.jatan.space/our-moon)|a curated page of resources for all earthlings to explore our cosmic neighbor, and learn about its fundamental importance to science and technology.|useful|
|Meiker|[https://meiker.io/](https://meiker.io/)|a doll customiser, like picrew, but in english!|fun|
|Goblin Bet|[https://goblin.bet/](https://goblin.bet/)|a little online game where users bet gold on randomized matches between monsters from dungeons and dragons.|fun|
|z0r|[https://z0r.de/](https://z0r.de/)|a website that showcases flash loops|fun|
|Floor 796|[https://floor796.com/](https://floor796.com/)|an ever-expanding animation scene showing the life of the 796th floor of a huge space station|fun|
|deceptive design|[https://www.deceptive.design/](https://www.deceptive.design/)|presents tricks used in websites and apps that make you do things that you didn't mean to|useful|
|pico8|[https://www.lexaloffle.com/pico-8.php](https://www.lexaloffle.com/pico-8.php)|a fantasy console for making, sharing and playing tiny games and other computer programs|fun|
|hacker typer|[https://hackertyper.com/](https://hackertyper.com/)|type like a true h4xx0r|fun|
|library of babel|[https://libraryofbabel.info/](https://libraryofbabel.info/)|a library that contains every piece of literature ever written, and every piece that could ever be written|fun|
|cursed text|[https://lingojam.com/CursedText](https://lingojam.com/CursedText)|cursed text|useful|
|Game accessibility guidelines|[https://gameaccessibilityguidelines.com/](https://gameaccessibilityguidelines.com/)|a repository of accessibility features you can put in your games.|serious|
|ichi city|[https://ichi.city/](https://ichi.city/)|little internet community where you can create your homepage, explore your creativity, and discover other people.|useful|
|Array in a Matrix|[https://arrayinamatrix.xyz/](https://arrayinamatrix.xyz/)|array's personal website, blog and random projects. hosts a matrix homeserver and a gemini capsule.|personal|
|Arcade Idea|[https://arcadeidea.wordpress.com/](https://arcadeidea.wordpress.com/)|a blog by art maybury about art analysis and video game history|fun|
|sudoku xv|[https://keeri.place/sudoku-xv.html](https://keeri.place/sudoku-xv.html)|a chill puzzle game|fun|
|based cooking|https://based.cooking/|a simple online cookbook without ads and obese web design|useful|
|YouGlish|[https://youglish.com/](https://youglish.com/)|a search engine that lets you search spoken words in youtube videos. great for learning how to speak english but also great for just finding relevant keywords!|useful|
|Large Horse|[http://large.horse/](http://large.horse/)|large horse|fun|
|VOLE.wtf|[https://vole.wtf/](https://vole.wtf/)|games, art, nonsense and crisp sandwiches.|fun|
|Free Music Archive|[https://freemusicarchive.org/](https://freemusicarchive.org/)|a large archive of music, most of it under creative commons.|useful|
|CPNK|[https://seapunk.xyz/](https://seapunk.xyz/)|the internet's batcave. we have tea and charcuterie.|personal|
|The Anarchist Library|[https://theanarchistlibrary.org/](https://theanarchistlibrary.org/)|a very expansive collection of anarchist texts.|serious|
|Exportify|[https://watsonbox.github.io/exportify/](https://watsonbox.github.io/exportify/)|export your spotify playlists into text|useful|
|The Bezier Game|[https://bezier.method.ac/](https://bezier.method.ac/)|a game that teaches you how to use the bezier tool.|useful|
|Wobble Town|[https://wobble.town/](https://wobble.town/)|a virtual town where people adopt and care for tiny pixelated creatures called wobbles.|fun|
|The Video Game Soda Machine Project|[https://vgsmproject.com/](https://vgsmproject.com/)|an attempt to catalog screenshots of every soda vending machine that has ever appeared in a video game.|fun|
|Jacob Online|[http://jacobonline.net/](http://jacobonline.net/)|my own little personal space for me to learn html and css, like a little blog! definetley have a look around|personal|
|evergreen's site|[https://itsevergreen.rip/](https://itsevergreen.rip/)|personal site of evergreen.|personal|
|notpron|[http://www.notpron.com/](http://www.notpron.com/)|the hardest riddle available on the internet|fun|
|JCPH DigiShell|[https://digishell.neocities.org/](https://digishell.neocities.org/)|a web desktop environment made by fasduswisdom that looks and feels like the early 2000s.|useful|
|Animal Art Reference|[https://x6ud.github.io/#/](https://x6ud.github.io/#/)|a bunch of 3d, freely rotatable animal skulls for artist reference.|useful|
|Blender|[https://www.blender.org/](https://www.blender.org/)|free open source 3d modelling software that does it all and then some, amazing piece of software.|useful|
|Coolors|[https://coolors.co/](https://coolors.co/)|colour palette generator.|useful|
|volvox vault|[https://volvoxvault.com/](https://volvoxvault.com/)|volvox is a thoughtful creative space.|social|
|95revive|[https://95revive.neocities.org/](https://95revive.neocities.org/)|a new website for your old browsers. welcome back to 1996!|personal|
|Piero Scaruffi|[https://www.scaruffi.com/](https://www.scaruffi.com/)|piero scaruffi’s knowledge base, the oldest personal website that is still active.|personal|
|ROCKTYPE|[https://rocktype.neocities.org/](https://rocktype.neocities.org/)|a funky webpage that talks about d&d, ocs, art, and videogames! also hosts a webring that shows off website layouts.|personal|
|Vampires!|[https://quiz.ravenblack.net/blood.pl?biter=RavenBlack](https://quiz.ravenblack.net/blood.pl?biter=RavenBlack)|a social website-style game where you lure people to click a link, and they get bitten and become vampires|social|
|thiswebsitewillselfdestruct|[https://www.thiswebsitewillselfdestruct.com/](https://www.thiswebsitewillselfdestruct.com/)|this website stays alive by being submitted  anonymous messages. a good resource.|healing|
|Privacy.Sexy|[https://privacy.sexy/](https://privacy.sexy/)|an user-friendly repository with quick and easy scripts for cleaning up your windows or mac and increasing your privacy and security.|useful|
|Piskel|[https://www.piskelapp.com/](https://www.piskelapp.com/)|make animated pixel art for free in your browser|useful|
